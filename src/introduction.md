Última atualização: 21/12/2020 

# Introdução

Este guia pressupõe que você tenha algum conhecimento básico da linguagem de programação rust.

Sempre que possível, estarei inserindo links de fontes oficiais.

Antes de inciar este guia de fato, devo falar porque escolhi a linguagem programação rust e não outra.

Como o próprio site oficial do rust diz<sup>[1](https://www.rust-lang.org/pt-BR/)</sup>
> Uma linguagem capacitando todos a construir softwares confiáveis e eficientes.

# Motivações

Pela grande falta de materiais em português, "novos", fáceis de entender e de serem encontrados e até mesmo para iniciantes que querem entender como uma linguagem funcionar por baixo dos "panos" mas tem dificuldade de entender materiais em outros idiomas.

Algumas diretrizes de início e para se inspirar:
> "Se você não sabe como funcionam os compiladores, não sabe como funcionam os computadores. Se você não tem 100% de certeza se sabe como funcionam os compiladores, então não sabe como eles funcionam."<sup>[2](http://steve-yegge.blogspot.com/2007/06/rich-programmer-food.html)</sup>

> "Se você não consegue explicar algo em termos simples, você não entende."<sup>[3](https://skeptics.stackexchange.com/questions/8742/did-einstein-say-if-you-cant-explain-it-simply-you-dont-understand-it-well-en)</sup>

# Por que rust?

Existe várias grandezas que são muito importantes de serem levadas em conta na hora de escolher uma tecnologia para desenvolver um determinado projeto:

## Desempenho
> Rust é extremamente rápido e gerencia memória eficientemente: sem runtime ou garbage collector, podendo potencializar a performance de serviços críticos, rodar em sistemas embarcados, e facilmente integrar-se a outras linguagens.

## Confiabilidade
> O rico sistema de tipos de Rust e seu modelo de ownership garantem segurança de memória e segurança de concorrência — e permite que você elimine muitas categorias de erros durante a compilação.

## Produtividade
> Rust possui uma ótima documentação, um compilador amigável com mensagens de erros úteis, e ferramental de primeira qualidade — uma ferramenta integrada de compilação e gerenciamento de pacotes, suporte inteligente para múltiplos editores com autocompleção e inspeções de tipos, um formatador automático, e muito mais.

## Vantagens

Mais três vantagens:
- A primeira é que o rust é uma linguagem compilada, o que é muito importante. O executável final terá muito mais desempenho se comparado com um executável de um programa escrito em um linguagem interpretada, pois muitas incluem no binário final todo ou boa parte do seu runtime que interpretará seu código.
- A segunda é que quando você fizer o release do projeto, o rust fará muitas otimizações, deixando seu executável ainda mais rápido.
- A terceira, o rust possui um dos se não o melhor compilador existente. Quando você compilar seu programa, o rust irá verificar seu código em busca de falhas, e irá lhe avisar que você pode fazer alterações no código para ter mais otimização, por exemplo, tirar variáveis que nunca serão usadas, etc.

Bom, com essas descrições e vantagens, o rust já entra na lista de tecnologias possíveis para a criação de uma linguagem.

Outras características do rust é:
- **Linha de Comando** Monte uma ferramenta de linha de comando rapidamente com o ecossistema robusto de Rust.
- **WebAssembly** Um grande destaque, podemos escrever programas para serem executados no navegador tornando seu javascript mais poderoso.
- **Redes** Desempenho previsível. Pouco uso de recursos. Alta confiabilidade.
- **Embarcados** Precisando de controle baixo nível sem desistir de conveniências de alto nível? Rust é a solução.

Espero que só com essas características, você tenha olhado com mais apreciação para o rust.


# Alguns tópicos que serão discutidos nos próximos capítulos
Não necessariamente na mesma ordem.
- Porque escrever uma linguagem inicialmente toda em código puro?
- Criando nossa primeira linguagem chamada `Calc`.
  - Suportará inicialmente apenas adição e subtração simples de inteiros.
  - Criar componentes em código rust puro:
    - `Lexer` para análise léxica.
    - `Parser` para análise sintática.
    - `Evaluator` para execução do programa.
    - `Object` para representar todos os tipos de dados.
    - `Env` para armazenar valores do programa, por exemplo variáveis.

